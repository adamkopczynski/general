# Internety – zasoby

## HTML + CSS
* CodeCademy [HTML & CSS | Codecademy](https://www.codecademy.com/learn/web), [Learn how to make a Website | Course | Codeacademy](https://www.codecademy.com/learn/make-a-website)
* [PL] Khan Academy  [Wstęp do HTML/CSS: Tworzenie stron internetowych |Khan Academy](https://pl.khanacademy.org/computing/computer-programming/html-css)
* FreeCodeCamp [Learn to Code and Help Nonprofits | freeCodeCamp](https://www.freecodecamp.org/)
* MarkSheet.io [MarkSheet: a free HTML and CSS tutorial - Free tutorial to learn HTML and CSS](http://marksheet.io/)
* [PL] Moja pierwsza strona w HTML5 i CSS3 [Moja pierwsza strona internetowa w HTML5 i CSS3 - darmowa książka do nauki HTML5 i CSS3](http://ferrante.pl/books/html/)
* [$$$] PluralSight  [HTML5 | Pluralsight](https://app.pluralsight.com/paths/skills/html5)]
* [PL] Kodu.je [HTML w praktyce #1 - wprowadzenie - YouTube](https://www.youtube.com/watch?v=uZLXBZalPcg&list=PL5nf3UIj1JtUwEgjEPo9LurVKKX5bH1IP)
* Learn to Code HTML & CSS [Learn to Code HTML & CSS](http://learn.shayhowe.com/html-css/)

## JavaScript
* [PL] CodeCombat [CodeCombat - Learn how to code by playing a game](https://codecombat.com/)
* Practical JavaScript [Practical JavaScript | Watch and Code®](https://watchandcode.com/p/practical-javascript)
* FreeCodeCamp [Learn to Code and Help Nonprofits | freeCodeCamp](https://www.freecodecamp.org/)
* CodeAcademy [Learn JavaScript | Codecademy](https://www.codecademy.com/learn/learn-javascript)
* JavaScript for Cats [JavaScript for Cats](http://jsforcats.com/)
* JavaScript30 [JavaScript 30 — Build 30 things with vanilla JS in 30 days with 30 tutorials](https://javascript30.com/)
* [$$$] CodeSchool [Learn JavaScript | Code School](https://www.codeschool.com/learn/javascript)

## Zaawansowany JavaScript
* JavaScript Guide [JavaScript Guide - JavaScript | MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
* You Don’t Know JS [You-Dont-Know-JS/README.md at master · getify/You-Dont-Know-JS · GitHub](https://github.com/getify/You-Dont-Know-JS/blob/master/README.md)
* Eloquent JavaScript [Eloquent JavaScript](http://eloquentjavascript.net/)
* JavaScript Enlightenment [JavaScript Enlightenment | by Cody Lindley | 1st Edition | ECMA-262, Edition 3](http://www.javascriptenlightenment.com/)
* Dla konkretnych tematów: egghead.io [JavaScript Tutorials and Video Workshops - @eggheadio #levelup](https://egghead.io/courses)